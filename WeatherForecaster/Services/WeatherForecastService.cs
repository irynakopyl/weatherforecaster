﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using WeatherForecaster.Models;

namespace WeatherForecaster.Services
{
    public class WeatherForecastService
    {
        readonly string apiKey = ConfigurationManager.AppSettings["apiKey"];
        readonly string apiUrl = ConfigurationManager.AppSettings["apiUrl"];
        public async Task<Weather> GetCityForecastAsync(string city)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var coordinates = JsonConvert.DeserializeObject<CityCoordinates>(await
                    (await httpClient.GetAsync(apiUrl + $"/weather?q={city}&appid={apiKey}")).
                    EnsureSuccessStatusCode().
                    Content.ReadAsStringAsync());
                if (coordinates.Name != city) throw new ArgumentException("Something went wrong...");
                {
                    var weather = JsonConvert.DeserializeObject<Forecast>(await
                    (await httpClient.GetAsync(apiUrl + $"/onecall?lat={coordinates.Coord.Lat}&lon={coordinates.Coord.Lon}&units=metric&exclude=minutely,hourly,current,alerts&appid={apiKey}")).
                    EnsureSuccessStatusCode().
                    Content.ReadAsStringAsync());
                    return weather.Daily.FirstOrDefault();
                }
            }
        }
    }
}
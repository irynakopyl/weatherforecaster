﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WeatherForecaster.Models;
using WeatherForecaster.Services;

namespace WeatherForecaster.Controllers
{
    public class HomeController : Controller
    {
        WeatherForecastService weatherForecastService = new WeatherForecastService();
        DateTime today = DateTime.Now.Date;
        public async Task<ActionResult> Index()
        {
            var city = HttpContext.Request.Cookies["city"];
            if (city == null) return View();

            ViewBag.City = HttpContext.Request.Cookies["city"].Value;
            var forecast = await weatherForecastService.GetCityForecastAsync(city.Value);
            SendNotification(city.Value, forecast);
            return View(forecast);
        }
        [HttpPost]
        public async Task<ActionResult> Index(string city)
        {
            try
            {
                if (!ModelState.IsValid || city == "" || city == null)
                {

                    ViewBag.Errors = "Print a city";
                    var archivedCity = HttpContext.Request.Cookies["city"];
                    if (archivedCity == null) return View();

                    ViewBag.City = HttpContext.Request.Cookies["city"].Value;
                    var weather = await weatherForecastService.GetCityForecastAsync(archivedCity.Value);
                    return View(weather);
                }
                var forecast = await weatherForecastService.GetCityForecastAsync(city);
                HttpContext.Response.Cookies["city"].Value = city;
                ViewBag.City = city;
                SendNotification(city, forecast);
                return View(forecast);
            }
            catch (Exception e)
            {
                ViewBag.Errors = e.Message;
                var archivedCity = HttpContext.Request.Cookies["city"];
                if (archivedCity == null) return View();

                ViewBag.City = HttpContext.Request.Cookies["city"].Value;
                var weather = await weatherForecastService.GetCityForecastAsync(archivedCity.Value);
                return View(weather);
            }
        }
        private void SendNotification(string city, Weather weather)
        {
            if (weather.Rain != 0)
            {
                Dictionary<string, DateTime> notifications;
                if (Session["notifications"] != null)
                {
                    notifications = Session["notifications"] as Dictionary<string, DateTime>;
                    if (!notifications.Where(x => x.Key == city && x.Value.Date == DateTime.Now.Date).Any())
                    {
                        notifications.Add(city, DateTime.Now);
                        Session["notifications"] = notifications;
                        ViewBag.Notification = "You are going to have a rain at:";
                    }
                }
                else
                {
                    notifications = new Dictionary<string, DateTime>();
                    notifications.Add(city, DateTime.Now.Date);
                    Session["notifications"] = notifications;
                    ViewBag.Notification = "You are going to have a rain at:";
                }
            }
        }
    }
}
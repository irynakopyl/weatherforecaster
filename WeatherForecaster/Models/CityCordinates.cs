﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Web;

namespace WeatherForecaster.Models
{
    public class CityCoordinates
    {
        public string Name { get; set; } 
        public Coordinate Coord{ get; set; }
    }
}

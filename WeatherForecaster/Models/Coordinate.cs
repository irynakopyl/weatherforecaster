﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherForecaster.Models
{
    public class Coordinate
    {
        public double Lon { get; set; }
        public double Lat { get; set; }
    }
}
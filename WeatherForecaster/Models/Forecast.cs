﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherForecaster.Models
{
    public class Forecast
    {
        public List<Weather> Daily { get; set; }
    }
}
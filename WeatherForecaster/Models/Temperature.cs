﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherForecaster.Models
{
    public class Temperature
    {
        public double Min { get; set; }
        public double Max { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherForecaster.Models
{
    public class Weather
    {
        public string Dt { get; set; }
        public Temperature Temp { get; set; }
        public double Pop { get; set; }
        public double Rain { get; set; }
    }
}
